======================
CC Python3
======================

Modified cookiecutter template for a Python package. The original templates can be found at
https://github.com/audreyr/cookiecutter and
https://github.com/Nekroze/cookiecutter-pypackage.

* Free software: BSD license
* Pytest_ runner: Supports `unittest`, `pytest`, `nose` style tests and more
* Tox_ testing: Setup to easily test for python 2.6, 2.7, 3.3 and PyPy_
* Sphinx_ docs: Documentation raedy for generation with, for example, ReadTheDocs_
* Wheel_ support: Use the newest python package distribution standard from the get go

Usage
-----

Generate a Python package project::

    cookiecutter https://gitlab.com/loxosceles/cc_python3

Then:

* Create a repo and put it there.
* Add the repo to your ReadTheDocs account + turn on the ReadTheDocs service hook.
* Run `tox` to make sure all tests pass.
* Release your package the standard Python way.
